FROM php:5.6-alpine

# Step 1.1: Install xdebug...
# Install xdebug
RUN apk --update add --virtual .build-deps alpine-sdk autoconf \
    && pecl install xdebug-2.5.0 \
    && apk del .build-deps

# Step 1.2: ...and enable it
# Enable xdebug
RUN docker-php-ext-enable xdebug
RUN touch /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_enable=1'           >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.profiler_enable=1'         >> /usr/local/etc/php/php.ini

# Some tweaks
RUN echo 'date.timezone = "Europe/Paris"'   >> /usr/local/etc/php/php.ini