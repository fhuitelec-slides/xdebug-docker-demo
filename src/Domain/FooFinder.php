<?php

namespace FHuitelec\Xdebug\Domain;

use FHuitelec\Xdebug\Domain\Collection\Foos;

interface FooFinder
{
    /** @return Foos */
    public function findAll();
}