<?php

namespace FHuitelec\Xdebug\Domain\Entity;

class Foo
{
    /** @var string */
    private $bar;

    /**
     * @param $bar string
     */
    public function __construct($bar)
    {
        $this->bar = $bar;
    }

    /** @return string|void */
    public function getBar()
    {
        $this->bar;
    }
}