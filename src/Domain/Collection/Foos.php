<?php

namespace FHuitelec\Xdebug\Domain\Collection;

use Assert\Assert;
use FHuitelec\Xdebug\Domain\Entity\Foo;

class Foos implements \IteratorAggregate
{
    /** @var array */
    private $foos;

    /**
     * @param array $foos
     */
    private function __construct(array $foos = [])
    {
        $this->foos = $foos;
    }

    public function getAllBars()
    {
        return array_map(function (Foo $foo) {
            return $foo->getBar();
        }, $this->foos);
    }

    public static function fromFoos(Foo ...$foos)
    {
        return new self($foos);
    }

    public static function fromArray(array $foos)
    {
        array_walk($foos, function($supposedFoo) {
            Assert::that($supposedFoo)
                ->isInstanceOf(
                    Foo::class,
                    sprintf('Your foo array must only contains %s classes', Foo::class)
                );
        });

        return new self($foos);
    }

    /**
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Foo[]|\ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->foos);
    }
}