<?php

namespace FHuitelec\Xdebug\Infrastructure\Finder;

use FHuitelec\Xdebug\Domain\Collection\Foos;
use FHuitelec\Xdebug\Domain\Entity\Foo;
use FHuitelec\Xdebug\Domain\FooFinder;

class ArrayEmojiFooFinder implements FooFinder
{
    /** @var string[] */
    private $emojis = [
        '💔',
        '🐞',
        '🐳',
        '🤘'
    ];

    /** @return Foos */
    public function findAll()
    {
        // Todo: Move this into Foos::fromStrings()
        $fooArray = array_map(function ($emoji) {
            return new Foo($emoji);
        }, $this->emojis);

        return Foos::fromArray($fooArray);
    }
}