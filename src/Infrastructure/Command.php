<?php

namespace FHuitelec\Xdebug\Infrastructure;

use FHuitelec\Xdebug\Domain\FooFinder;

class Command
{
    /** @var FooFinder */
    private $fooFinder;

    /**
     * @param FooFinder $fooFinder
     */
    public function __construct(FooFinder $fooFinder)
    {
        $this->fooFinder = $fooFinder;
    }

    public function run()
    {
        $foos = $this->fooFinder->findAll();

        foreach ($foos as $idx => $foo) {
            echo sprintf("Foo %d: %s\n", $idx + 1, $foo->getBar());
        }
    }
}