<?php

namespace specs\FHuitelec\Xdebug\Infrastructure\Finder;

use FHuitelec\Xdebug\Infrastructure\Finder\ArrayEmojiFooFinder;
use PhpSpec\ObjectBehavior;

class ArrayEmojiFooFinderSpec extends ObjectBehavior
{
    public function it is initializable()
    {
        $this->shouldHaveType(ArrayEmojiFooFinder::class);
    }
}