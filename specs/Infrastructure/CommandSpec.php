<?php

namespace specs\FHuitelec\Xdebug\Infrastructure;

use FHuitelec\Xdebug\Domain\FooFinder;
use FHuitelec\Xdebug\Infrastructure\Command;
use PhpSpec\ObjectBehavior;

class CommandSpec extends ObjectBehavior
{
    public function let(FooFinder $fooFinder)
    {
        $this->beConstructedWith($fooFinder);
    }

    public function it is initializable()
    {
        $this->shouldHaveType(Command::class);
    }
}