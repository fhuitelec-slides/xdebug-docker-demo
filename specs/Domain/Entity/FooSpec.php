<?php

namespace specs\FHuitelec\Xdebug\Domain\Entity;

use FHuitelec\Xdebug\Domain\Entity\Foo;
use PhpSpec\ObjectBehavior;

class FooSpec extends ObjectBehavior
{
    public function let()
    {
        $this->beConstructedWith('🤘');
    }

    public function it is initializable()
    {
        $this->shouldHaveType(Foo::class);
    }

    public function it returns bar()
    {
        $this->getBar()->shouldReturn('🤘');
    }
}