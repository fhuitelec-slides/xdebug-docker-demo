#!/usr/bin/env php

<?php

use FHuitelec\Xdebug\Infrastructure\Command;
use FHuitelec\Xdebug\Infrastructure\Finder\ArrayEmojiFooFinder;

require __DIR__ . '/../vendor/autoload.php';

// IoC
$emojiFinder = new ArrayEmojiFooFinder();

// Run
(new Command($emojiFinder))
    ->run();