#!/usr/bin/env make

export DOCKER_EXEC = docker-compose -p foobar -f infrastructure/development/docker-compose.yml

.PHONY: docker-run
docker-run:
	$(DOCKER_EXEC) up -d --build

.PHONY: docker-down
docker-down:
	$(DOCKER_EXEC) down

.PHONY: docker-php
docker-php:
	$(DOCKER_EXEC) exec app sh

.PHONY: docker-specs
docker-specs:
	$(DOCKER_EXEC) exec app sh -c 'cd /app && ./vendor/bin/phpspec run'