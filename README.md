# Xdebug & docker simple demo

More documentation coming soon :kissing:

## Getting started

### Initialization

1. Copy `docker-compose` `.env` file
```bash
cp .env.dist .env
```

2. Create alias interface (tested on macOS only)
```bash
# Adresse IP de classe A arbitraire
# Attention aux collisions
sudo ifconfig lo0 alias 10.0.2.2
```

3. Add `REMOTE_HOST=10.0.2.2` in `.env`

### Running

```bash
# Run the stack
make docker-run
# Enter the application container
make docker-php
## In the PHP container
composer install --dev
exit
## On your host
# Run specs tests
make docker-specs
```